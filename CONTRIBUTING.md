When starting work on something, fork the project, create a branch and work on it.
You can either immediately create a merge request and prefix it with `WIP:` to indicate 
 that you're still working on it, or finish you work on a branch and create a merge request.


# Documentation

The documentation is written for [Sphinx].
In order to generate it locally, run `make html`.
You can then open the resulting [index.html][doc/index].

## Additional tools 

The wireframes are made with [draw.io Desktop][drawio_desktop].

The graphs are made with [graphviz] which has a [online editor][graphviz online].

[doc/index]: build/html/index.html
[drawio_desktop]: https://github.com/jgraph/drawio-desktop
[graphviz]: https://graphviz.org/documentation/
[graphviz online]: https://dreampuf.github.io/GraphvizOnline
[Sphinx]: https://www.sphinx-doc.org
