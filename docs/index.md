Welcome to EduGraph's documentation!
===================================

**Understanding is just as important as knowing**

[EduGraph](https://gitlab.com/NamingThingsIsHard/social/education/edugraph>) is short for *Educational Graph*, which is education using knowledge graphs.
Well... graphs won't be used to teach you stuff, just to help you find things to learn.

We don't want to require users to create an account, so the majority of the website will be available
to anybody visiting it.
People should be able to create an account in order to track progress, courses they are interested in,
exams they would like to task, and to of course contribute content like tests, courses, materials,
lectures, etc.


The goal of this project is to provide a website that allows users to find topics they want to learn
and get a quick overview of what it is they need to know to master the topic.
Additionally, users who don't know what it is they want to learn should be able to find topics based on their knowledge.

This is supposed to be enabled using a [knowledge graph](core/knowledge_graph.md).
Each node on the graph will provide materials to learn like videos, exercises, tests and articles.

Features
--------

- Content discovery by simple search
- Discover topics to learn based on current knowledge/understanding
- Build a learning path to a specific goal and keep track of their achievements, knowledge and understand
- Take courses with tests
- Rate courses, videos and quizzes/exams

And more.
We will try and document everything that should be possible with the platform.
