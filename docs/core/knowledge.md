Knowledge
=========

Even though we talk about knowledge a lot
There are two types of knowledge that the platform possesses:

System knowledge
----------------

[//]: # (TODO find a good name)

These are the things that users can learn.
Building that storage of knowledge will be a major challenge of this app.

User knowledge
--------------

This is simply what users know and their understanding / level thereof.
When using the platform, some feature will depend hereon.
Users are able to provide that information.

Of course user knowledge will have to be assessed which is discussed in [assessment](assessment.md).

What good is this knowledge?
----------------------------

*Knowledge is power*

Connecting the two types of knowledge is what should drive learning.
We should be able to see what it is we don't know and understand.
Then we can map a path there.

A path to bridge the gap between a user's current understanding and the target understanding
doesn't always have to be the shortest one there.
Users might want to learn certain other things (related or unrelated) along the way,
so calculating the path will be possible with the [Knowledge Graph](./knowledge_graph.md).

How is this knowledge collected
--------------------------------

[//]: # (TODO)
