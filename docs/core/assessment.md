Assessment
==========

Depending on the assessment level, users will be able to get different badges.

1.Self assessment
-----------------

Users assess their own aptitude and the app takes them at their word.
This only has meaning to the app which will use it to decide what topics a user needs to focus on
in order to reach a certain level of understanding.

These assessments are not shown on the user's public profile.

2.Peer assessment
-----------------

Peers will be able to create tests for others to take.
Multiple choice tests can be taken and automatically assessed by the system,
but others will have to be reviewed by peers.


Test rating
^^^^^^^^^^^

Once a test has been successfully completed, the user will be able to rate it.
Depending on the rating, a different aptitude level could be calculated.

The following points could be evaluated:

**Difficulty**

"Very Easy", "Easy", "OK", "Hard", "Very Hard"

**Satisfaction**

How satisfied the user was with the test.

.. note::
    More points will be added in order to help users find good tests.

3.Accredited assessment
-----------------------

This will be the highest level of assessment available to users.
Certain users or accounts will have been assessed and accredited in defined fashion
to give it maximum credibility.
Tests provided by these accredited users or accounts should be able to give a much better confirmation
on the level of mastery a user has on a subject.


