#!/bin/bash
set -euo pipefail

DEB_FILE=/tmp/drawio.deb
DEB_URL=https://github.com/jgraph/drawio-desktop/releases/download/v13.9.9/draw.io-amd64-13.9.9.deb

echo "Downloading draw.io deb"
wget -qcO "$DEB_FILE" "$DEB_URL"

echo "Installing drawio deps"
apt-get update -qqq
apt-get install -y -qqqq \
    xvfb \
    libnotify4 \
    libappindicator3-1 \
    libsecret-1-0 \
    libdbusmenu-glib4 \
    libdbusmenu-gtk3-4 \
    libsecret-common \
    libnss3 \
    libgbm1 \
    libasound2 \
    xdg-utils

echo "install drawio"
dpkg -i "$DEB_FILE"

echo "Cleaning up"
rm "$DEB_FILE"
